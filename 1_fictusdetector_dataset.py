# 1_fictusdetector_dataset.py
# Author: Diego Yoshiro Dongo Esquivel
# 2023

!pip install config

import tweepy
import json
from datetime import datetime
import pandas as pd

from config import *
from google.colab import files

"""Credenciales"""

consumer_key = "[your-credentials]"
consumer_secret = "[your-credentials]"
bearer_token = "[your-credentials]"
access_token = "[your-credentials]"
access_token_secret = "[your-credentials]"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

"""Declaracion de variables"""

# Usuarios de Twitter de los noticieros confiables y no
noticierosTrue = ["noticiAmerica", "Cuarto_Poder", "atv_noticias", "DiaDatv", "DOMINGOALDIA", "Latina_Noticias", "PanoramaPTV", "noticias_tvperu", "atvmasnoticias"]
noticierosFake = ["PolloFarsantePe", "justicierope", "el_analista2020", "blackdragon1802", "Sinmermelada3", "derecha_Unida_", "elcomentaristam", "reyreysincorona", "eljokerpe",
                  "vekace82_laley", "elanalista2022", "PIKANTEDEKUY2", "padre_n", "justicierojope", "AntifachosPE"]
# Baneados: "malditaternura", "jcd46"
# Beneados - 8enero : "peru_memoria"
nro_min_palabras = 40

"""Modulos"""

# Guarda las noticias
def saveNewsCsv(noticieros, nroTweets, fileName, date_since):
  news_list = []
  for noticieroT in noticieros:
    print(noticieroT)
    # Usamos algunos parámetros extra para excluir respuestas, retweets y traer el texto completo de cada tweet
    for status in tweepy.Cursor(api.user_timeline, screen_name=noticieroT, exclude_replies=True,
                                include_rts=False, tweet_mode='extended').items(nroTweets):
        # Agregamos el texto, fecha, likes, retweets y hashtags al array
        if (len(status.full_text.split(" ")) > nro_min_palabras and fechaDentroRango(status.created_at, date_since)):
          news_list.append([status.full_text, status.created_at, status.id])
  # Convertimos el array en un DataFrame y nombramos las columnas
  news_list = pd.DataFrame(news_list, columns=['text', 'created_at', 'id'])
  # Guardamos en el directorio en que estamos trabajando
  news_list.to_csv(fileName)
  files.view(fileName)

# Une archivos .csv
def concatCsvLabeliza(fileTrue, fileFake, fileCsvExportName):
  readfileTrue = pd.read_csv(fileTrue)
  readfileFake = pd.read_csv(fileFake)
  col1 = "label"
  col2 = "text"
  col3 = "created_at"
  col4 = "id"
  dataTrue = pd.DataFrame({col1: "true", col2:readfileTrue.loc[:, "text"], col3:readfileTrue.loc[:, "created_at"], col4:readfileTrue.loc[:, "id"]})
  dataFake = pd.DataFrame({col1: "fake", col2:readfileFake.loc[:, "text"], col3:readfileFake.loc[:, "created_at"], col4:readfileFake.loc[:, "id"]})
  dataGeneral = pd.concat([dataTrue, dataFake])
  dataGeneral.to_csv(fileCsvExportName, index=False, encoding='utf-8')
  files.view(fileCsvExportName)
  files.download(fileCsvExportName)

# Verifica si una fecha es mayor que desde
def fechaDentroRango(fechaTweet, fechaDesdeString):
  # fechaTweet = datetime.strptime(fechaTweet, '%Y-%m-%d %H:%M:%S')
  fechaDesde = datetime.strptime(fechaDesdeString, '%Y-%m-%d')
  if (fechaTweet > fechaDesde):
    return True
  else:
    return False

"""Generacion de .csv"""

saveNewsCsv(noticierosTrue, 1500, "datasetTrue.csv", "2022-11-21")
saveNewsCsv(noticierosFake, 500, "datasetFake.csv", "2022-11-21")

# Concat datasets

concatCsvLabeliza("datasetTrue.csv", "datasetFake.csv", "dataset.csv")
