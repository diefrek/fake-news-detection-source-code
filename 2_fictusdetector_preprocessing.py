# 2_fictusdetector_preprocessing.py
# Author: Diego Yoshiro Dongo Esquivel
# 2023

import re
import copy
import pandas as pd
import numpy as np
import nltk

from google.colab import files
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

nltk.download('stopwords')
nltk.download('punkt')

"""Upload Dataset"""

uploaded = files.upload()
for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'
  .format(encoding='utf-8', name=fn, length=len(uploaded[fn])))

"""Leer Dataset"""

fileName = "datasetRecuperado (5).csv"

df = pd.read_csv(fileName, encoding='latin-1')
print(df.shape)
print(df)

"""Duplicar columna"""

df = df.fillna('')

#Crea otra columna llenada con el contenido de las otras celdas
df['text_tokenizado'] = df['text']
df.head()

"""Etiquetas diferentes"""

#Se muestran todas las posibilidades de label diferentes de vacio sin repetir
df = df[df['label'] != '']
print(df['label'].unique())

"""Conteo de noticias"""

#Realiza un conteo de FAKES y TRUES
no_of_fakes = df.loc[df['label'] == 'fake'].count()[0]
no_of_trues = df.loc[df['label'] == 'true'].count()[0] + df.loc[df['label'] == 'True'].count()[0] + df.loc[df['label'] == 'TRUE'].count()[0]
print("Nro Fake: ", no_of_fakes)
print("Nro True: ", no_of_trues)

"""Modulos"""

##### Text preprocessing
stop_words = set(stopwords.words('spanish'))
stop_words.add("rt")

def clean(text):
    # Lowering letters
    text = text.lower()
    #el re es para expresiones regulares
    # Removing html tags
    text = re.sub(r'<[^>]*>', '', text)
    # Removing twitter usernames
    text = re.sub(r'@\w+', '', text)
    # Removing hashtags
    text = re.sub(r'#\w+', '', text)
    # Removing urls
    # text = re.sub(r'(\w+)://([\w\-\.]+)/(\w+).(\w+)', ' ', text)
    text = re.sub(r'https?://\S+', '', text)
    # Removing numbers and punctuation
    text = re.sub('[^a-zA-ZÀ-ÿ]', ' ', text)
    #Tokenizar las palabras del texto
    word_tokens = word_tokenize(text)
    #Se agregan las palabras que no son stop words en  filtered_sentence
    filtered_sentence = []
    for word_token in word_tokens:
        if (word_token not in stop_words):
            filtered_sentence.append(word_token)
    # Joining words
    text = (' '.join(filtered_sentence))
    # text = [token for token in filtered_sentence if len(token) > 1]
    return text

"""Prueba de limpieza"""

print(clean('🔴𝐀𝐄𝐑𝐎𝐏𝐔𝐄𝐑𝐓𝐎 @asdas @adminasdf34a 𝐃𝐄 #𝐂𝐇𝐈𝐍𝐂𝐇𝐄𝐑𝐎En abril, RT 44 mu3rt3 se #RechazoSalvaAChile <b> taghtml</b> 2233iniciará la remoción de tierras a cargo del Consorcio Chinchero conformado https://t.co/R6sios3IVg por #Hyunday Construcción e Ingeniería y HBV Contratistas. Más detalles: https://t.co/kAw679F78z https://t.co/kAw679F78z https://t.co/kAw679F78z'))

"""Limpieza de cada celda"""

#Se limpia el texto de cada celda
df['text_tokenizado'] = df['text'].apply(lambda x: clean(x))
df[['text', 'text_tokenizado']].head()
df.to_csv("datasetProcessed.csv", index=False, encoding='utf-8')
files.view("datasetProcessed.csv")
