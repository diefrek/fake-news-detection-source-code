# 3_fictusdetector_featureextraction.py
# Author: Diego Yoshiro Dongo Esquivel
# 2023

import numpy as np
import cvxopt
from sklearn.datasets import make_blobs
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix

from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
from google.colab import files

import re
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords

# Para analisis sentimental
!pip install sentiment-analysis-spanish

from sentiment_analysis_spanish import sentiment_analysis
sentiment = sentiment_analysis.SentimentAnalysisSpanish()

# Para non dictionary words
!pip install autocorrect

from autocorrect import Speller
spell = Speller(lang='es')

# Para Hate Speech
!pip install pysentimiento

from pysentimiento import create_analyzer
hate_speech_analyzer = create_analyzer(task="hate_speech", lang="es")
emotion_analyzer = create_analyzer(task="emotion", lang="es")
sentiment_analyzer = create_analyzer(task="sentiment", lang="es")
# ner_analyzer = create_analyzer("ner", lang="es")
pos_tagger = create_analyzer("pos", "es")

#Para wordCloud
!pip install wordcloud
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt

"""Cargar dataset"""

uploaded = files.upload()
for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'
  .format(encoding='utf-8', name=fn, length=len(uploaded[fn])))

fileName = "dataset2 v1.1.csv"
fileName = "datasetFeaturesIncompletov3.csv"
fileName = "datasetProcessedRecuperado.csv"
fileName = "datasetFeaturesNaive v4.csv"

df = pd.read_csv(fileName, encoding='utf-8')
print(df.shape)
df.head()
colorTrue="#006ea0"
colorFake="#b22222"

"""Sentimental Analysis (No da resultados confiables con el texto en bruto)"""

def analysisSentimental(text):
    puntuacion = sentiment.sentiment(text)
    return puntuacion

#Crea otra columna llenada con el analisis sentimental del texto limpio
print(analysisSentimental('Yo no, estoy muy feliz en mi trabajo'))
print(analysisSentimental('Yo no estoy muy feliz en mi trabajo'))
print(analysisSentimental('No! Estoy triste porque los tengo cerca!'))
print(analysisSentimental('No estoy triste, porque los tengo cerca'))

"""Non Dictionary Words"""

def nonDictionaryWords(text):
  text_split = text.split(" ")
  count_non_dictionary = 0
  total_words = len(text_split)
  for word in text_split:
    if (word != spell(word)):
      count_non_dictionary += 1
  return  count_non_dictionary/total_words

#Crea otra columna llenada con el puntaje de non dictionary words

"""Hate Speech"""

def hateSpeechHateful(text):
  hateful = hate_speech_analyzer.predict(text).probas["hateful"]
  return hateful

def hateSpeechTargeted(text):
  targeted = hate_speech_analyzer.predict(text).probas["targeted"]
  return targeted

def hateSpeechAggressive(text):
  aggressive = hate_speech_analyzer.predict(text).probas["aggressive"]
  return aggressive

# df['Hateful'] = (df['text_tokenizado'].astype('U')).apply(hateSpeechHateful)
df['Hateful'] = (df['text'].astype('U')).apply(hateSpeechHateful)

# df['Targeted'] = (df['text_tokenizado'].astype('U')).apply(hateSpeechTargeted)

# df['Aggressive'] = (df['text_tokenizado'].astype('U')).apply(hateSpeechAggressive)
df['Aggressive'] = (df['text'].astype('U')).apply(hateSpeechAggressive)
df.head(8)

"""Emotion"""

def emotionAnalysisFear(text):
  fear = emotion_analyzer.predict(text).probas["fear"]
  return fear

def emotionAnalysisAnger(text):
  anger = emotion_analyzer.predict(text).probas["anger"]
  return anger

def emotionAnalysisSurprise(text):
  surprise = emotion_analyzer.predict(text).probas["surprise"]
  return surprise

def emotionAnalysisSadness(text):
  sadness = emotion_analyzer.predict(text).probas["sadness"]
  return sadness

def emotionAnalysisJoy(text):
  joy = emotion_analyzer.predict(text).probas["joy"]
  return joy

def emotionAnalysisDisgust(text):
  disgust = emotion_analyzer.predict(text).probas["disgust"]
  return disgust

def emotionAnalysisOthers(text):
  others = emotion_analyzer.predict(text).probas["others"]
  return others

#Recorrer uno por uno, es demasiado
df['Fear'] = (df['text'].astype('U')).apply(emotionAnalysisFear)

# df['Others'] = (df['text'].astype('U')).apply(emotionAnalysisOthers)

df['Surprise'] = (df['text'].astype('U')).apply(emotionAnalysisSurprise)

df['Sadness'] = (df['text'].astype('U')).apply(emotionAnalysisSadness)

df['Anger'] = (df['text'].astype('U')).apply(emotionAnalysisAnger)

df['Joy'] = (df['text'].astype('U')).apply(emotionAnalysisJoy)

df['Disgust'] = (df['text'].astype('U')).apply(emotionAnalysisDisgust)

"""Sentimental 2, es mejor con CONTEXTO"""

def positiveSpeechHateful(text):
  positive = sentiment_analyzer.predict(text).probas["POS"]
  return positive

def negativeSpeechHateful(text):
  negative = sentiment_analyzer.predict(text).probas["NEG"]
  return negative

def neutralSpeechHateful(text):
  neutral = sentiment_analyzer.predict(text).probas["NEU"]
  return neutral

df['Positive'] = (df['text'].astype('U')).apply(positiveSpeechHateful)
df['Negative'] = (df['text'].astype('U')).apply(negativeSpeechHateful)
# df['Neutral'] = (df['text'].astype('U')).apply(neutralSpeechHateful)
df['Neutral'] = (1 - df['Positive'] - df['Negative'])
df.head()

"""Se exporta el archivo"""

df.to_csv("datasetFeatures.csv", index=True, encoding='utf-8')
files.view("datasetFeatures.csv")
files.download("datasetFeatures.csv")

"""Se guarda el conocimiento utilizando Pickle"""

import pickle
import joblib

hate_speech_analyzer = create_analyzer(task="hate_speech", lang="es")
emotion_analyzer = create_analyzer(task="emotion", lang="es")
sentiment_analyzer = create_analyzer(task="sentiment", lang="es")


# Guardar el modelo
data = {"hate_speech_analyzer": hate_speech_analyzer,
        "emotion_analyzer": emotion_analyzer,
        "sentiment_analyzer": sentiment_analyzer}

with open('svm_analizer.joblib', 'wb') as f:
    joblib.dump(data, f)

files.download("svm_analizer.joblib")

"""**Graficos**"""

#WordCloud
concat_text_true = ''
concat_text_false = ''
for i in range(len(df)):
  # print(df.iloc[i]['text_tokenizado'])
  if (df.iloc[i]['label'] == 'TRUE' or df.iloc[i]['label'] == 'true'):
    concat_text_true = concat_text_true + ' ' + str(df.iloc[i]['text_tokenizado'])
  else:
    concat_text_false = concat_text_false + ' ' + str(df.iloc[i]['text_tokenizado'])

print(concat_text_true)
print(len(concat_text_true))

print(concat_text_false)
print(len(concat_text_false))

own_stop_words = ["tdt", "tras", "movistar", "star", "claro", "globalcom", "best", "cable", "jorge", "ballón", "televisión",
                  "dba"]

wc = WordCloud(width=1000, height=1000, background_color="white", stopwords=own_stop_words,
               max_words=40, min_word_length=3, prefer_horizontal=0.5).generate(concat_text_true)
plt.figure(figsize=(10, 10))
plt.axis("off")
plt.imshow(wc)

wc = WordCloud(width=1000, height=1000, background_color="white", stopwords=own_stop_words,
               max_words=40, min_word_length=3, prefer_horizontal=0.5).generate(concat_text_false)
plt.figure(figsize=(10, 10))
plt.axis("off")
plt.imshow(wc)

a = np.random.normal(0, 3, 1000)
b = np.random.normal(2, 4, 900)

bins = np.linspace(-10, 10, 50)

plt.hist(a, bins, alpha = 0.5, label='a')
plt.hist(b, bins, alpha = 0.5, label='b')
plt.legend(loc='upper left')

plt.show()

# Distribución de los tweets por longitud
# ==============================================================================
fig, ax = plt.subplots(figsize=(9,5))
# fig, ax = plt.(figsize=(9,5))

def longitudString(s):
  return len(s.split(' '))


label = 'true'
df_temp = df[df['label'] == label].copy()
df_temp['Longitud'] = (df_temp['text'].astype('U')).apply(longitudString)
df_temp = df_temp.groupby(df_temp['Longitud']).size()
df_temp.plot(label=label, ax=ax, linewidth=2, color=colorTrue)

label = 'fake'
df_temp = df[df['label'] == label].copy()
df_temp['Longitud'] = (df_temp['text'].astype('U')).apply(longitudString)
df_temp = df_temp.groupby(df_temp['Longitud']).size()
df_temp.plot(label=label, ax=ax, linewidth=2, color=colorFake)

ax.set_ylabel('Cantidad');
# ax.set_title('Número de noticias por longitud')
ax.set_xlim(40, 60)
ax.legend();

"""POS Tagging"""

# Part of Speech Tagging

def partOfSpeech(text):
  pos_list = pos_tagger.predict(text)
  adj_count = 0
  verb_count = 0
  noun_count = 0
  pron_count = 0
  propn_count = 0
  for i in range (len(pos_list)):
    if (pos_list[i]['type']=='ADJ'):
      adj_count = adj_count + 1
    if (pos_list[i]['type']=='VERB'):
      verb_count = verb_count + 1
    if (pos_list[i]['type']=='NOUN'):
      noun_count = noun_count + 1
    if (pos_list[i]['type']=='PRON'):
      pron_count = pron_count + 1
    if (pos_list[i]['type']=='PROPN'):
      propn_count = propn_count + 1
  # print(adj_count)
  # print(verb_count)
  # print(noun_count)
  # print(pron_count)
  # print(propn_count)
  return verb_count, adj_count, noun_count

def partOfSpeechSpecific(text, pos):
  pos_list = pos_tagger.predict(text)
  pos_count = 0
  for i in range (len(pos_list)):
    if (pos_list[i]['type']==pos):
      pos_count = pos_count + 1
  print(pos_count)
  return pos_count

def stringLength(s):
  return len(s.split(' '))



# partOfSpeech("Me llamo Juan Manuel Pérez y vivo en Argentina")
partOfSpeech("Hola mundo")
partOfSpeechSpecific("Hola mundo", "NOUN")

# Distribución de los tweets por proporcion de stop words
# ==============================================================================
fig, ax = plt.subplots(figsize=(9,5))
fig2, ax2 = plt.subplots(figsize=(9,5))
fig3, ax3 = plt.subplots(figsize=(9,5))

#=====================================================
label = "true"
df_temp = df[df['label'] == label].copy()
pos_count  = (df_temp['text'].astype('U')).apply(partOfSpeech)

verb_count = [int(fila[0]) for fila in pos_count]
adj_count = [int(fila[1]) for fila in pos_count]
noun_count = [int(fila[2]) for fila in pos_count]

text_long = (df_temp['text'].astype('U')).apply(stringLength)
verb_proportion = round(verb_count/text_long, 1)
adj_proportion = round(adj_count/text_long, 1)
noun_proportion = round(noun_count/text_long, 1)

df_temp['verb_prop'] = verb_proportion
df_temp['adj_prop'] = adj_proportion
df_temp['noun_prop'] = noun_proportion

df_temp_verb = df_temp.groupby(df_temp['verb_prop']).size()
df_temp_verb.plot(label=label, ax=ax, linewidth=2, color=colorTrue)

df_temp_adj = df_temp.groupby(df_temp['adj_prop']).size()
df_temp_adj.plot(label=label, ax=ax2, linewidth=2, color=colorTrue)

df_temp_noun = df_temp.groupby(df_temp['noun_prop']).size()
df_temp_noun.plot(label=label, ax=ax3, linewidth=2, color=colorTrue)

#=====================================================
label="fake"
df_temp = df[df['label'] == label].copy()
pos_count  = (df_temp['text'].astype('U')).apply(partOfSpeech)

verb_count = [int(fila[0]) for fila in pos_count]
adj_count = [int(fila[1]) for fila in pos_count]
noun_count = [int(fila[2]) for fila in pos_count]

text_long = (df_temp['text'].astype('U')).apply(stringLength)
verb_proportion = round(verb_count/text_long, 1)
adj_proportion = round(adj_count/text_long, 1)
noun_proportion = round(noun_count/text_long, 1)

df_temp['verb_prop'] = verb_proportion
df_temp['adj_prop'] = adj_proportion
df_temp['noun_prop'] = noun_proportion

df_temp_verb = df_temp.groupby(df_temp['verb_prop']).size()
df_temp_verb.plot(label=label, ax=ax, linewidth=2, color=colorFake)

df_temp_adj = df_temp.groupby(df_temp['adj_prop']).size()
df_temp_adj.plot(label=label, ax=ax2, linewidth=2, color=colorFake)

df_temp_noun = df_temp.groupby(df_temp['noun_prop']).size()
df_temp_noun.plot(label=label, ax=ax3, linewidth=2, color=colorFake)

# =================================================

ax.set_title('# Noticias por proporción de verbos')
ax.set_ylabel('Cantidad');
ax.legend();

ax2.set_title('# Noticias por proporción de adjetivos')
ax2.set_ylabel('Cantidad');
ax2.legend();

ax3.set_title('# Noticias por proporción de sustantivos')
ax3.set_ylabel('Cantidad');
ax3.legend();

"""Frecuencia de Stop Words"""

# Distribución de los fecuencia de SW
# ==============================================================================
fig, ax = plt.subplots(figsize=(9,5))

def longitudString(s):
  return len(s.split(' '))

#===============================================================
label = "true"
df_temp = df[df['label'] == label].copy()

text_len  = (df_temp['text'].astype('U')).apply(longitudString)
text_tokenizado_len  = (df_temp['text_tokenizado'].astype('U')).apply(longitudString)

stop_words_prop = round(text_tokenizado_len/text_len, 1)

df_temp['stop_words_prop'] = stop_words_prop
df_temp = df_temp.groupby(df_temp['stop_words_prop']).size()
df_temp.plot(label=label, ax=ax, linewidth=2, color=colorTrue)
#=============================================================
label = "fake"
df_temp = df[df['label'] == label].copy()

text_len  = (df_temp['text'].astype('U')).apply(longitudString)
text_tokenizado_len  = (df_temp['text_tokenizado'].astype('U')).apply(longitudString)

stop_words_prop = round(text_tokenizado_len/text_len, 1)

df_temp['stop_words_prop'] = stop_words_prop
df_temp = df_temp.groupby(df_temp['stop_words_prop']).size()
df_temp.plot(label=label, ax=ax, linewidth=2, color=colorFake)
#==============================================================

ax.set_title('Número de noticias por proporcion de SW')
ax.set_ylabel('Cantidad');
ax.legend();