# 4_fictusdetector_naivebayes.py
# Author: Diego Yoshiro Dongo Esquivel
# 2023

import copy
import pandas as pd

from sklearn.model_selection import train_test_split
from google.colab import files

"""Upload Dataset"""

uploaded = files.upload()
for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'
  .format(encoding='utf-8', name=fn, length=len(uploaded[fn])))

"""Leer Dataset"""

fileName = "datasetFeatures v4.csv"
fileName = "datasetFeaturesNaive v4.csv"
df = pd.read_csv(fileName, encoding='utf-8')
print(df.shape)
df.head()

"""Nro de noticias por etiqueta"""

y = df['label']
y[y == "true"] = "true"
y[y == "TRUE"] = "true"
y[y == "fake"] = "fake"
y[y == "FAKE"] = "fake"
no_of_fakes = df.loc[df['label'] == 'fake'].count()[0]
no_of_trues = df.loc[df['label'] == 'true'].count()[0]
print("Nro noticias fake: ", no_of_fakes)
print("Nro noticias true: ", no_of_trues)

"""Modulos"""

def histogram(hist_word, hist_count, text):
  text = text.split()
  for word in text:
    if word not in hist_word:
      hist_word.append(word)
      hist_count.append(1)
  return hist_word, hist_count

def histogram2(hist_word, hist_count, hist_prob, nro_words, text):
    text = text.split()
    for word in text:
        if word in hist_word:
            hist_count[hist_word.index(str(word))] = hist_count[hist_word.index(str(word))] + 1
        else:
            hist_word.append(word)
            hist_count.append(1)
    return hist_word, hist_count, hist_prob

# Metodo que determina la probabilidad de una palabra en un histograma
def probability_word(word, hist_word, hist_count, nro_words):
    # Suponiendo que la palabra siempre estara en el histograma
    pos = hist_word.index(word)
    count = hist_count[pos]
    probability = count / nro_words
    return probability

def determinar_prob(text, hist_word, hist_prob):
  prob = 1
  decimales = 0
  text = text.split()
  for word in text:
      for k in range (0,len(hist_word)-1):
            if (word == hist_word[k]):
              if (hist_prob[k] > 0):
                prob = prob * hist_prob[k]
                while (prob < 0.01):
                  prob = prob * 100
                  decimales = decimales + 1
  return prob, decimales

def prediccion(texto, hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes):
  #Prediccion
  prob_text_T, nro_decimales_T = determinar_prob(texto, hist_word_T, hist_prob_T)
  prob_text_F, nro_decimales_F = determinar_prob(texto, hist_word_F, hist_prob_F)
  prob_text_T = prob_text_T * (no_of_trues/ (no_of_trues + no_of_fakes))
  prob_text_F = prob_text_F * (no_of_fakes/ (no_of_trues + no_of_fakes))

  if (nro_decimales_T > nro_decimales_F):
    return "fake"
  elif (nro_decimales_T == nro_decimales_F):
      if (prob_text_T > prob_text_F):
        return "true"
      else:
        return "fake"
  else:
    #print("REAL")
    return "true"

def prediccion_test(X_test, y_test, hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes):
  vv = 0
  vf = 0
  fv = 0
  ff = 0
  pred = ''
  for k in range (0, len(X_test)-1):
    pred = prediccion(X_test[k], hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes)
    # Si la prediccion es correcta
    if (y_test[k] == pred):
      if (y_test[k] == "true"):
        # Si es un verdadero etiquetado como verdadero
        vv = vv + 1
      else:
        # Si es un falso etiquetado como falso
        ff = ff + 1
    else:
      if (y_test[k] == "true"):
        # Si es un falso etiquetado como verdadero
        fv = vf + 1
      else:
        # Si es un verdadero etiquetado como falso
        vf = vf + 1
  total = vv + vf + fv + ff
  print("verdadero etiquetado como verdadero: ",vv, " ", vv/(vv + vf))
  print("falso etiquetado como falso        : ",ff, " ", ff/(fv + ff))
  print("verdadero etiquetado como falso    : ",vf, " ", vf/(vv + vf))
  print("falso etiquetado como verdadero    : ",fv, " ", fv/(fv + ff))

  return ((vv + ff) / (vv + vf + fv + ff))

"""Se splitea el texto y se cuentan las palabras"""

df['splited_text'] = df['text_tokenizado'].str.split(' ')
df['n_words'] = df['splited_text'].str.len()
# df['n_Words'] = df['processed_Text'].str.split(',').str.len()
df.head(5)

"""Declaracion de variables"""

# Variables utilizadas para las probablidades de Fake y Rial news
hist_word = []
hist_count = []
hist_prob = []
nro_words = 0
hist_word_T = []
hist_count_T = []
hist_prob_T = []
nro_words_T = 0
hist_word_F = []
hist_count_F = []
hist_prob_F = []
nro_words_F = 0

"""Se separa el dataset en traning y testing"""

X = df['text_tokenizado'].values
# X = df['splited_text'].values
y = df['label'].values

X_train, X_test, y_train, y_test = train_test_split(X, y, shuffle=True, test_size=0.2, random_state=11)

"""Generacion de histogramas"""

#Recuperar un elemento dada una fila y columna, las filas se cuentan desde 0
print(df.iloc[2]['n_words'])

#recuperar nro de filas sin contar head
print('Row count is:',df.shape[0])

print(hist_word)
print(hist_count)
print(hist_prob)

print("True antes")
print(hist_word_T)
print(hist_count_T)
print(hist_prob_T)

print("Fake antes")
print(hist_word_F)
print(hist_count_F)
print(hist_prob_F)


for k in range (0, len(X_train)-1):
  texto = str(X_train[k])
  hist_word, hist_count = histogram(hist_word, hist_count, texto)

hist_word_T = copy.copy(hist_word)
hist_count_T = copy.copy(hist_count)
hist_word_F = copy.copy(hist_word)
hist_count_F = copy.copy(hist_count)

#Se determina el histograma para las noticias verdaderas y falsas
for k in range (0, len(X_train)-1):
  texto = str(X_train[k])
  if (y_train[k] == 'true'):
    hist_word_T, hist_count_T, hist_prob_T = histogram2(hist_word_T, hist_count_T, hist_prob_T, nro_words_T, texto)
  else:
    hist_word_F, hist_count_F, hist_prob_F = histogram2(hist_word_F, hist_count_F, hist_prob_F, nro_words_F, texto)

print("True despues")
print(hist_word_T)
print(hist_count_T)
print(hist_prob_T)

print("Fake despues")
print(hist_word_F)
print(hist_count_F)
print(hist_prob_F)

print('Row count is:',len(hist_word))

"""Calculo de probabilidades"""

#Para la probabilidad
# estas no sirven porque son numero de oraciones
print(no_of_trues)
print(no_of_fakes)

nro_words_T = 0
nro_words_F = 0

for k in range (0, len(hist_count)-1):
  nro_words_T = nro_words_T + hist_count_T[k]
  nro_words_F = nro_words_F + hist_count_F[k]

print(nro_words_T)
print(nro_words_F)

for word in hist_word_T:
    hist_prob_T.append(probability_word(word, hist_word_T, hist_count_T, nro_words_T))

for word in hist_word_F:
    hist_prob_F.append(probability_word(word, hist_word_F, hist_count_F, nro_words_F))

print("True despues")
print(hist_word_T)
print(hist_count_T)
print(hist_prob_T)

print("Fake despues")
print(hist_word_F)
print(hist_count_F)
print(hist_prob_F)

"""Matriz de confusion"""

#Predicion para matriz confusion
precision = prediccion_test(X_test, y_test, hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes)
print("precision: ", precision)

"""Se guarda el conocimiento utilizando Pickle"""

import pickle

# prediccion(texto, hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes)

# Guardar el modelo
data = {"hist_word_T": hist_word_T,
        "hist_prob_T": hist_prob_T,
        "hist_word_F": hist_word_F,
        "hist_prob_F": hist_prob_F,
        "no_of_trues": no_of_trues,
        "no_of_fakes": no_of_fakes}
with open('naive_bayes_analizer.pickle', 'wb') as f:
    pickle.dump(data, f)

files.download("naive_bayes_analizer.pickle")

"""Falta la parte para la caracteristica de signo"""

#========================================================
def prediccionSigno(texto, hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes):
  #Prediccion
  prob_text_T, nro_decimales_T = determinar_prob(texto, hist_word_T, hist_prob_T)
  prob_text_F, nro_decimales_F = determinar_prob(texto, hist_word_F, hist_prob_F)

  #OJOOO se esta multiplicando por la probabilidad de la cantidad de verdaderas y falsas mas
  prob_text_T = prob_text_T * (no_of_trues/ (no_of_trues + no_of_fakes))
  prob_text_F = prob_text_F * (no_of_fakes/ (no_of_trues + no_of_fakes))

  return prob_text_T, nro_decimales_T, prob_text_F, nro_decimales_F

#Almacenar los valores en un Excel
from openpyxl.chart import ScatterChart, LineChart, Reference, Series
from datetime import datetime
from openpyxl.styles.borders import Border, Side

# Se declaran arreglos para nuevas columnas
prob_text_T2_array = []
nro_decimales_T2_array = []
prob_text_F2_array = []
nro_decimales_F2_array = []
signo_array = []
signo_nor_sin_array = []
signo_nor_con_array = []
signo_divison_array = []

# Se calcula por filas
for k in range (0, len(X)):
  prob_text_T2, nro_decimales_T2, prob_text_F2, nro_decimales_F2 = prediccionSigno(str(X[k]), hist_word_T, hist_prob_T, hist_word_F, hist_prob_F, no_of_trues, no_of_fakes)

  prob_text_T2_array.append(prob_text_T2)
  nro_decimales_T2_array.append(nro_decimales_T2)
  prob_text_F2_array.append(prob_text_F2)
  nro_decimales_F2_array.append(nro_decimales_F2)

  if (nro_decimales_T2 > nro_decimales_F2):
    valor_signo =  prob_text_F2*-1
    valor_signo_menor = prob_text_T2
  elif (nro_decimales_T2 == nro_decimales_F2):
    if (prob_text_T2 > prob_text_F2):
      valor_signo =  prob_text_T2
      valor_signo_menor = prob_text_F2*-1
    else:
      valor_signo =  prob_text_F2*-1
      valor_signo_menor = prob_text_T2
  else:
    valor_signo =  prob_text_T2
    valor_signo_menor = prob_text_F2*-1

  #Para calcular el multiplicador
  potencia = abs(nro_decimales_T2-nro_decimales_F2)

  signo_array.append(valor_signo)
  signo_nor_sin_array.append((valor_signo/valor_signo_menor)*(-1)*(10**(potencia)))

  if (valor_signo < 0):
    #Es negativo
    signo_nor_con_array.append((valor_signo/valor_signo_menor)*(10**(potencia)))
    signo_divison_array.append((valor_signo/valor_signo_menor))

  else:
    #Es positivo
    signo_nor_con_array.append((valor_signo/valor_signo_menor)*(-1)*(10**(potencia)))
    signo_divison_array.append((valor_signo/valor_signo_menor)*(-1))

# Se agregan las nuevas columnas
df['ProbT'] = prob_text_T2_array
df['NroDecT'] = nro_decimales_T2_array
df['ProbF'] = prob_text_F2_array
df['NroDecF'] = nro_decimales_F2_array
df['Signo'] = signo_array
df['SignoNorSin'] = signo_nor_sin_array
df['SignoNorCon'] = signo_nor_con_array
df['SignoDivision'] = signo_divison_array

"""Se exporta el archivo"""

df.to_csv("datasetFeaturesNaive v4.csv", index=True, encoding='utf-8')
files.view("datasetFeaturesNaive v4.csv")
files.download("datasetFeaturesNaive v4.csv")