# 5_fictusdetector_svm.py
# Author: Diego Yoshiro Dongo Esquivel
# 2023

import numpy as np
import cvxopt
from sklearn.datasets import make_blobs
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix
from sklearn import metrics

from google.colab import files
import pandas as pd
import re

from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords

uploaded = files.upload()
for fn in uploaded.keys():
  print('User uploaded file "{name}" with length {length} bytes'
  .format(encoding='utf-8', name=fn, length=len(uploaded[fn])))

fileName = "datasetFeaturesNaive v4.csv"


df = pd.read_csv(fileName, encoding='utf-8')
print(df.shape)
df.head()

import numpy as np
from numpy import linalg
import cvxopt
import cvxopt.solvers

def linear_kernel(x1, x2):
    return np.dot(x1, x2)

def polynomial_kernel(x, y, p=3):
    return (1 + np.dot(x, y)) ** p

def gaussian_kernel(x, y, sigma=5.0):
    return np.exp(-linalg.norm(x-y)**2 / (2 * (sigma ** 2)))

class SVM3(object):

    def __init__(self, kernel=linear_kernel, C=None):
        self.kernel = kernel
        self.C = C
        if self.C is not None: self.C = float(self.C)

    def fit(self, X, y):
        n_samples, n_features = X.shape

        # Gram matrix
        K = np.zeros((n_samples, n_samples))
        for i in range(n_samples):
            for j in range(n_samples):
                K[i,j] = self.kernel(X[i], X[j])

        P = cvxopt.matrix(np.outer(y,y) * K)
        q = cvxopt.matrix(np.ones(n_samples) * -1)
        A = cvxopt.matrix(y, (1,n_samples))
        b = cvxopt.matrix(0.0)

        if self.C is None:
            G = cvxopt.matrix(np.diag(np.ones(n_samples) * -1))
            h = cvxopt.matrix(np.zeros(n_samples))
        else:
            tmp1 = np.diag(np.ones(n_samples) * -1)
            tmp2 = np.identity(n_samples)
            G = cvxopt.matrix(np.vstack((tmp1, tmp2)))
            tmp1 = np.zeros(n_samples)
            tmp2 = np.ones(n_samples) * self.C
            h = cvxopt.matrix(np.hstack((tmp1, tmp2)))

        # solve QP problem
        solution = cvxopt.solvers.qp(P, q, G, h, A, b)

        # Lagrange multipliers
        a = np.ravel(solution['x'])

        # Support vectors have non zero lagrange multipliers
        sv = a > 1e-5
        ind = np.arange(len(a))[sv]
        self.a = a[sv]
        self.sv = X[sv]
        self.sv_y = y[sv]
        # print("%d support vectors out of %d points" % (len(self.a), n_samples))

        # Intercept
        self.b = 0
        for n in range(len(self.a)):
            self.b += self.sv_y[n]
            self.b -= np.sum(self.a * self.sv_y * K[ind[n],sv])
        self.b /= len(self.a)

        # Weight vector
        if self.kernel == linear_kernel:
            self.w = np.zeros(n_features)
            for n in range(len(self.a)):
                self.w += self.a[n] * self.sv_y[n] * self.sv[n]
        else:
            self.w = None

    def project(self, X):
        if self.w is not None:
            return np.dot(X, self.w) + self.b
        else:
            y_predict = np.zeros(len(X))
            for i in range(len(X)):
                s = 0
                for a, sv_y, sv in zip(self.a, self.sv_y, self.sv):
                    s += a * sv_y * self.kernel(X[i], sv)
                y_predict[i] = s
            return y_predict + self.b

    def predict(self, X):
        return np.sign(self.project(X))

if __name__ == "__main__":
    import pylab as pl

    def plot_margin(X1_train, X2_train, clf):
        def f(x, w, b, c=0):
            # given x, return y such that [x,y] in on the line
            # w.x + b = c
            return (-w[0] * x - b + c) / w[1]

        #====================================  BORRAR
        pl.figure(figsize=(12,9))

        pl.plot(X1_train[:,0], X1_train[:,1], "ro")
        pl.plot(X2_train[:,0], X2_train[:,1], "bo")
        pl.scatter(clf.sv[:,0], clf.sv[:,1], s=100, c="g")

        # w.x + b = 0
        a0 = -4; a1 = f(a0, clf.w, clf.b)
        b0 = 4; b1 = f(b0, clf.w, clf.b)
        pl.plot([a0,b0], [a1,b1], "k")

        # w.x + b = 1
        a0 = -4; a1 = f(a0, clf.w, clf.b, 1)
        b0 = 4; b1 = f(b0, clf.w, clf.b, 1)
        pl.plot([a0,b0], [a1,b1], "k--")

        # w.x + b = -1
        a0 = -4; a1 = f(a0, clf.w, clf.b, -1)
        b0 = 4; b1 = f(b0, clf.w, clf.b, -1)
        pl.plot([a0,b0], [a1,b1], "k--")

        pl.axis("tight")
        pl.show()

    def plot_contour(X1_train, X2_train, clf):
        #====================================  BORRAR
        pl.figure(figsize=(12,9))

        pl.plot(X1_train[:,0], X1_train[:,1], "ro")
        pl.plot(X2_train[:,0], X2_train[:,1], "bo")
        pl.scatter(clf.sv[:,0], clf.sv[:,1], s=100, c="g")

        # X1, X2 = np.meshgrid(np.linspace(-6,6,50), np.linspace(-6,6,50))
        X1, X2 = np.meshgrid(np.linspace(-1,1,50), np.linspace(-1,1,50))
        X1, X2 = np.meshgrid(np.linspace(0,1,50), np.linspace(0,1,50))
        X = np.array([[x1, x2] for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
        Z = clf.project(X).reshape(X1.shape)
        pl.contour(X1, X2, Z, [0.0], colors='k', linewidths=1, origin='lower')
        pl.contour(X1, X2, Z + 1, [0.0], colors='grey', linewidths=1, origin='lower')
        pl.contour(X1, X2, Z - 1, [0.0], colors='grey', linewidths=1, origin='lower')

        pl.axis("tight")
        pl.show()

df.head()

X = pd.DataFrame(df, columns=["Disgust", "Signo"])

X = X.to_numpy()
y = df['label']
y[y == "true"] = 1
y[y == "TRUE"] = 1
y[y == "fake"] = -1
y = y.to_numpy()
y = y.astype(np.double)

c1 = 0
c2 = 0
for yaux in y:
  if yaux == -1:
    c1 = c1 + 1
  else:
    c2 = c2 + 1
print(c1)
print(c2)

# CON SIGNO2 ==================================================

plt.figure(figsize=(10,10))
# plt.scatter(X[:, 0], X[:, 1], c=y, cmap='bwr', alpha=0.45, label=y)

blue = '#003ba0'
red = '#b22222'

# Create scatter plots for each label
plt.scatter(X[y == 1, 0], X[y == 1, 1], c=blue, alpha=0.45, label='true')
plt.scatter(X[y == -1, 0], X[y == -1, 1], c=red, alpha=0.45, label='fake')

# Add labels and legend
plt.xlabel('Disgust', fontsize=14)
plt.ylabel('Signo', fontsize=14)
plt.legend(markerscale=2, fontsize=14)
plt.show()

plt.figure(figsize=(10,10))

blue = '#003ba0'
red = '#b22222'

# Create scatter plots for each label
plt.scatter(X[y == -1, 0], X[y == -1, 1], c=red, alpha=0.45, label='fake')

# Add labels and legend
plt.xlabel('Hateful', fontsize=14)
plt.ylabel('Disgust', fontsize=14)
plt.legend(markerscale=2, fontsize=14)
plt.show()

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.80, random_state=0)

c1 = 0
c2 = 0
for yaux in y_test:
  if yaux == -1:
    c1 = c1 + 1
  else:
    c2 = c2 + 1
print(c1)
print(c2)

"""TEST CON NOTICIA REAL"""

clf = SVM3(C=100)
clf.fit(X_train, y_train)

features_true_test = [0.5545336478424168, 0.05555555555555555, 0.01963050477206707, 0.018330959603190422, 0.0009504240588285029, 0.06181532144546509, 0.0010362613247707486, 0.9060274958610535, 0.006078914739191532, 0.03622175380587578, 0.9576992988586426]
features_true_test = [0.8429586172491141, 0.1111111111111111, 0.01207820326089859, 0.013623667880892754, 0.0025290304329246283, 0.00547434575855732, 0.0011041057296097279, 0.9805124998092651, 0.0037730408366769552, 0.050341181457042694, 0.9458857774734497]
features_true_test = [0.0033398473844954597, 0.0, 0.018034962937235832, 0.015623416751623154, 0.005023257341235876, 0.01499925833195448, 0.0018253076123073697, 0.9685792326927185, 0.006521913222968578, 0.011674805544316769, 0.9818031787872314]
features_true_test = [0.12868496513206015, 0.05, 0.05421547591686249, 0.029158800840377808, 0.0018162026535719633, 0.005474042613059282, 0.002248128643259406, 0.946248471736908, 0.0013298694975674152, 0.8668540120124817, 0.131816104054451]


features_fake_test = [0.01967386083103935, 0.2, 0.9749523401260376, 0.920698881149292, 0.6771097183227539, 0.004463063087314367, 0.15894077718257904, 0.13429899513721466, 0.000701588112860918, 0.9837485551834106, 0.015549843199551105]
features_fake_test = [2.451291899111277e-05, 0.0, 0.19183173775672913, 0.06609310209751129, 0.007632064167410135, 0.00474938889965415, 0.0037660100497305393, 0.954524576663971, 0.005106574390083551, 0.015290357172489166, 0.9796029925346375]
features_fake_test = [4.750630575832926e-05, 0.05, 0.2172885686159134, 0.07840847969055176, 0.06283267587423325, 0.0113795455545187, 0.022652538493275642, 0.4902387857437134, 0.011157984845340252, 0.35356220602989197, 0.6352797746658325]


features_no_se = [0.030221951077193946, 0.0, 0.01967449299991131, 0.014663441106677055, 0.007225566543638706, 0.011080507189035416, 0.009166259318590164, 0.8674158453941345, 0.004916350357234478, 0.6622458696365356, 0.3328377902507782]
features_no_se = [0.0003479188066654376, 0.10526315789473684, 0.11146660894155502, 0.057855620980262756, 0.015187435783445835, 0.0025625228881835938, 0.016054712235927582, 0.8827229738235474, 0.001765933004207909, 0.8454874753952026, 0.15274663269519806]

#6 correctas de 7

clf.predict(features_no_se)

# -1 es falso
# 1 es verdadero

from sklearn.svm import SVC
from sklearn import metrics
# kernel{‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’} or callable, default=’rbf’
# svc=SVC() #Default hyperparameters

cValue = 100

svc=SVC(C=cValue, kernel='linear')
svc.fit(X_train,y_train)
y_pred=svc.predict(X_test)
print('Accuracy Score:')
print(metrics.accuracy_score(y_test,y_pred))

svc=SVC(C=cValue, kernel='poly')
svc.fit(X_train,y_train)
y_pred=svc.predict(X_test)
print('Accuracy Score:')
print(metrics.accuracy_score(y_test,y_pred))

svc=SVC(C=cValue, kernel='rbf')
svc.fit(X_train,y_train)
y_pred=svc.predict(X_test)
print('Accuracy Score:')
print(metrics.accuracy_score(y_test,y_pred))

svc=SVC(C=cValue, kernel='sigmoid')
svc.fit(X_train,y_train)
y_pred=svc.predict(X_test)
print('Accuracy Score:')
print(metrics.accuracy_score(y_test,y_pred))

def test_soft_propio(X_train, X_test, y_train, y_test):
  clf = SVM3(kernel=linear_kernel, C=100)
  clf.fit(X_train, y_train)

  y_predict = clf.predict(X_test)
  correct = np.sum(y_predict == y_test)
  print("%d out of %d predictions correct" % (correct, len(y_predict)))
  print('Accuracy Score:')
  print(metrics.accuracy_score(y_test,y_predict))
  print(metrics.confusion_matrix(y_test,y_predict))

  plot_contour(X_train[y_train==1], X_train[y_train==-1], clf)

#======================================================
test_soft_propio(X_train, X_test, y_train, y_test)

"""Benchmarking"""

features = ["Sentimental", "NonDictionary", "Hateful", "Aggressive", "Anger", "Joy", "Disgust", "Others", "Positive", "Negative", "Neutral"]
# features = ["Sentimental", "NonDictionary", "Hateful"]

def getValoresEjes(feature1, feature2):
  X = pd.DataFrame(df, columns=[feature1, feature2])
  X = X.to_numpy()
  y = df['label']
  y[y == "true"] = 1
  y[y == "fake"] = -1
  y = y.to_numpy()
  y = y.astype(np.double)
  return X, y

def splitearDatos(X, y, train, random):
  X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=train, random_state=random)
  return X_train, X_test, y_train, y_test

def benchmark_soft(feature1, feature2, c_value, kernel_type, train, random):
  X, y = getValoresEjes(feature1, feature2)
  X_train, X_test, y_train, y_test = splitearDatos(X, y, train, random)
  clf = SVM3(kernel=kernel_type, C=c_value)
  clf.fit(X_train, y_train)
  y_predict = clf.predict(X_test)
  correct = np.sum(y_predict == y_test)
  # print("%d out of %d predictions correct" % (correct, len(y_predict)))
  print('Accuracy Score:')
  print(metrics.accuracy_score(y_test,y_predict))
  print('===================================================')
  # plot_contour(X_train[y_train==1], X_train[y_train==-1], clf)
  return metrics.accuracy_score(y_test,y_predict)

matriz = []
for feature1 in features:
  listaFila = []
  for feature2 in features:
    if (feature1 != feature2):
      print(feature1, feature2)
      accuracy = benchmark_soft(feature1, feature2, 1, gaussian_kernel, 0.80, 42)
      listaFila.append(accuracy)
    else:
      listaFila.append("-")
  matriz.append(listaFila)
print(matriz)

matriz = []
for feature1 in features:
  listaFila = []
  for feature2 in features:
    if (feature1 != feature2):
      print(feature1, feature2)
      accuracy = benchmark_soft(feature1, feature2, 10, gaussian_kernel, 0.80, 42)
      listaFila.append(accuracy)
    else:
      listaFila.append("-")
  matriz.append(listaFila)
print(matriz)

matriz = []
for feature1 in features:
  listaFila = []
  for feature2 in features:
    if (feature1 != feature2):
      print(feature1, feature2)
      accuracy = benchmark_soft(feature1, feature2, 100, gaussian_kernel, 0.80, 42)
      listaFila.append(accuracy)
    else:
      listaFila.append("-")
  matriz.append(listaFila)
print(matriz)

#==========================================
# Para todas las caracteristicas
X = pd.DataFrame(df, columns=["Hateful" , "Aggressive", "Fear", "Surprise", "Sadness", "Anger", "Joy", "Disgust", "Positive", "Negative", "Neutral", "Signo"])
# X = pd.DataFrame(df, columns=["Hateful" , "Aggressive", "Fear", "Surprise", "Sadness", "Anger", "Joy", "Disgust", "Positive", "Negative", "Neutral"])
X = X.to_numpy()
y = df['label']
y[y == "TRUE"] = 1
y[y == "true"] = 1
y[y == "fake"] = -1
y = y.to_numpy()
y = y.astype(np.double)
X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.9, random_state=42)

def test_soft_propio2(X_train, X_test, y_train, y_test, kernel, c):
  clf = SVM3(kernel=kernel, C=c)
  clf.fit(X_train, y_train)
  y_predict = clf.predict(X_test)
  correct = np.sum(y_predict == y_test)
  print("%d out of %d predictions correct" % (correct, len(y_predict)))
  print('Accuracy Score:')
  print(metrics.accuracy_score(y_test,y_predict))
  print(metrics.confusion_matrix(y_test,y_predict))

#======================================================
test_soft_propio2(X_train, X_test, y_train, y_test, kernel=linear_kernel, c=1)

kernel=polynomial_kernel
c = 100
clf = SVM3(kernel=kernel, C=c)
clf.fit(X_train, y_train)

print(clf)

import pickle

# Guardar el modelo
with open('fictus_detector_model.pickle', 'wb') as f:
    pickle.dump(clf, f)

files.download("fictus_detector_model.pickle")

#======================================================
test_soft_propio2(X_train, X_test, y_train, y_test, kernel=linear_kernel, c=10)

#======================================================
test_soft_propio2(X_train, X_test, y_train, y_test, kernel=linear_kernel, c=100)